﻿/// <reference path="jquery-2.2.2.js" />

$(document).ready(function () {
    
    $("#job_selection").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="167"){
                $("#job_selection div").not("#167-description").hide();
                $("#167-description").show();
            }
            else if($(this).attr("value")=="259"){
                $("#job_selection div").not("#259-description").hide();
                $("#259-description").show();
            }
            else if($(this).attr("value")=="313"){
                $("#job_selection div").not("#313-description").hide();
                $("#313-description").show();
            }
        });
    }).change();

    $("#next_to_employment").click(function () {
        $("#personal_section").hide();
        $("#employment_section").show();
        return false;
    });

    $("#next_to_employer").click(function () {
        $("#employment_section").hide();
        $("#employer_section").show();
        return false;
    });

    $("#next_to_reference").click(function () {
        $("#employer_section").hide();
        $("#reference_section").show();
        return false;
    });

    $("#previous_to_personal").click(function () {
        $("#employment_section").hide();
        $("#personal_section").show();
        return false;
    });

    $("#previous_to_employment").click(function () {
        $("#employer_section").hide();
        $("#employment_section").show();
        return false;
    });

    $("#previous_to_employer").click(function () {
        $("#reference_section").hide();
        $("#employer_section").show();
        return false;
    });

    $("#employer_minus").click(function () {
        $("#employer").hide();
    });

    $("#can-you-work-all").change(function () {
        $("#weekends, #evenings").prop('checked', $(this).prop("checked"));
    });

    $("#available-all").change(function () {
        $("#monday, #tuesday, #wednesday, #thursday, #friday, #saturday, #sunday").prop('checked', $(this).prop("checked"));
    });

    $("#eligible1").change(function () {
        if ($(this).attr("value") == "1") {
            $("#employment_information_question_1a").hide();
        }
    });

    $("#eligible0").change(function () {
        if ($(this).attr("value") == "0") {
            $("#employment_information_question_1a").show();
        }
    });

    $("#worked_here_before1").change(function () {
        if ($(this).attr("value") == "1") {
            $("#worked_here_previously").show();
        }
    });

    $("#job_desc_received1").change(function () {
            if ($(this).attr("value") == "1") {
                $("#understand_reqs0").removeAttr('disabled');
                $("#understand_reqs1").removeAttr('disabled');
            }
    });

    $("#job_desc_received0").change(function () {
        if ($(this).attr("value") == "0") {
            $("#understand_reqs0").prop( "disabled", true );
            $("#understand_reqs1").prop("disabled", true);
        }
    });

    $("#understand_reqs0").change(function () {
        if ($(this).attr("value") == "0") {
            $("#no_understand_text").show();
        }
    });

    $("#conf_agreement1").click(function () {
        $("#conf_explain_text").show();
    });
    $("#conf_agreement0").click(function () {
        $("#conf_explain_text").hide();
    });

    $("#discharged1").click(function () {
        $("#discharge_explain_text").show();
    });
    $("#discharged0").click(function () {
        $("#discharge_explain_text").hide();
    });

    $("#convict1").click(function () {
        $("#convict_explain_text").show();
    });
    $("#convict0").click(function () {
        $("#convict_explain_text").hide();
    });

    $("#family1").click(function () {
        $("#family_explain_text").show();
    });
    $("#family0").click(function () {
        $("#family_explain_text").hide();
    });

    $("form").validate({

        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            home_phone: {
                required: true,
                phoneUS: true,
            },
            work_phone: {
                phoneUS: true,
            },
            cell_phone: {
                phoneUS: true,
            },
            email_address: {
                required: true,
                email: true,
            },
            ssn: {
                required: true,
                ssnValidation: true,
            },
            street: {
                required: true,
            },
            city: {
                required: true,
            },
            state: {
                required: true,
                isstate: true,
            },
            zip_code: {
                required: true,
                zipcodeUS: true,
            },
            employer: {
                required: true,
            },
            employer_city: {
                required: true,
            },
            employer_state: {
                required: true,
                isstate: true,
            },
            employer_pos: {
                required: true,
            },
            employer_from: {
                required: true,
            },
            employer_to: {
                required: true,
            },
            employer_leave: {
                required: true,
            },
            ref1_name: {
                required: true,
            },
            ref1_email_address: {
                email: true,
            },
            ref1_relationship: {
                required: true,
            },
            ref1_years: {
                required: true,
            },

        }
    });
});

$.validator.addMethod("ssnValidation", function (value, index) {
    return value.match(/^([0-6]\d{2}|7[0-6]\d|77[0-2])([ \-]?)(\d{2})\2(\d{4})$/);

}, "Please enter a valid SSN");

$.validator.addMethod("isstate", function (value) {
    var states = [
        "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA",
        "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
        "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
        "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
        "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY",
        "AS", "DC", "FM", "GU", "MH", "MP", "PR", "PW", "VI"
    ];
    return $.inArray(value.toUpperCase(), states) != -1;
}, "Data provided is not a valid state");