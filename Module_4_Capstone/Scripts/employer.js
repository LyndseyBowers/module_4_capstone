﻿/// <reference path="jquery-2.2.2.js" />

$(document).ready(function () {

    $("#employer_minus").click(function () {
        $("#employer").hide();
    });

    $("#employer_plus").click(function () {
        $("#employer").show();
    });

    $("#employer1_plus").click(function () {
        $("#employer1").show();
    });

    $("#employer1_minus").click(function () {
        $("#employer1").hide();
    });

    $("#employer2_plus").click(function () {
        $("#employer2").show();
    });

    $("#employer2_minus").click(function () {
        $("#employer2").hide();
    });

    $("#employer3_plus").click(function () {
        $("#employer3").show();
    });

    $("#employer3_minus").click(function () {
        $("#employer3").hide();
    });
});